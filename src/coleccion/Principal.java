package coleccion;

import java.util.ArrayList;
import java.util.Collections;
//import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class Principal {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Empleado> leArrayList = new ArrayList<Empleado>();
		List<Empleado> leLinkedList = new LinkedList<Empleado>();
		List<Empleado> leVector = new Vector<Empleado>();
		Scanner leer = new Scanner(System.in);
		byte opcionList, numeroEmpleados;
		String nombre;
		int edad, altura;
		System.out.println("INGRESE EL NUMERO DE EMPLEADOS (MAXIMO 10)");
		numeroEmpleados = leer.nextByte();
		while (numeroEmpleados < 1 || numeroEmpleados > 10) {
			System.out.println("EL NUMERO DE EMPLEADOS ES INVALIDO!!!");
			System.out.println("INTENTE NUEVAMENTE");
			numeroEmpleados = leer.nextByte();
		}
		for (int i = 0; i < numeroEmpleados; i++) {
			System.out.println("------------------------------------------------------------------------------");
			System.out.print("INGRESE EL NOMBRE DEL EMPLEADO " + (i + 1) + ": ");
			nombre = leer.nextLine();
			nombre = leer.nextLine();
			System.out.print("INGRESE LA EDAD DEL EMPLEADO " + (i + 1) + " (19 A�OS - 80 A�OS): ");
			edad = leer.nextInt();
			while (edad < 19 || edad > 80) {
				System.out.print("!!!ERROR INGRESE LA EDAD DEL EMPLEADO " + (i + 1) + " (19 A�OS - 80 A�OS): ");
				edad = leer.nextInt();
			}
			System.out.print("INGRESE LA ALTURA DEL EMPLEADO " + (i + 1) + " EN CENTIMETROS (135cm - 200cm): ");
			altura = leer.nextInt();
			while (altura < 135 || altura > 200) {
				System.out.print(
						"!!!ERROR INGRESE LA ALTURA DEL EMPLEADO " + (i + 1) + " EN CENTIMETROS (135cm - 200cm): ");
				altura = leer.nextInt();
			}
			leArrayList.add(new Empleado(nombre, edad, altura));
			leLinkedList.add(new Empleado(nombre, edad, altura));
			leVector.add(new Empleado(nombre, edad, altura));
			System.out.println();
		}
		do {
			System.out.println("CON QUE TIPO DE LISTA DESEA ORDENAR?");
			System.out.println("1. ArrayList");
			System.out.println("2. LinkedList");
			System.out.println("3. Vector");
			System.out.println("0. salir");
			opcionList = leer.nextByte();
			switch (opcionList) {
			case 1:
				prioridad(leArrayList);
				break;
			case 2:
				prioridad(leLinkedList);
				break;
			case 3:
				prioridad(leVector);
				break;
			case 0:
				System.out.println("EL PROGRAMA HA FINALIZADO!!!");
				break;
			default:
				System.out.println("OPCION INVALIDA!!!");
			}
		} while (opcionList != 0);

	}

	private static void prioridad(List<Empleado> lista) {
		// TODO Auto-generated method stub
		Scanner leer = new Scanner(System.in);
		byte opcionOrganizar;
		do {
			System.out.println("QUE ATRIBUTO DESEA ORDENAR EN LA LISTA?");
			System.out.println("1. NOMBRE");
			System.out.println("2. EDAD");
			System.out.println("3. ALTURA");
			System.out.println("0. VOLVER");
			opcionOrganizar = leer.nextByte();
			switch (opcionOrganizar) {
			case 1:
				Collections.sort(lista, new OrdenarNombre());
				imprimir(lista);
				break;
			case 2:
				Collections.sort(lista, new OrdenarEdad());
				imprimir(lista);
				break;
			case 3:
				Collections.sort(lista, new OrdenarAltura());
				imprimir(lista);
				break;
			case 0:
				break;
			default:
				System.out.println("OPCION INVALIDA!!!");
			}
		} while (opcionOrganizar != 0);

	}

	private static void imprimir(List<Empleado> lista) {
		// TODO Auto-generated method stub
		for (Empleado elemento : lista) {
			System.out.println(elemento);
		}
	}

}
