package coleccion;

public class Empleado {
	private String nombre;
	private Integer edad;
	private Integer altura;

	public Empleado(String nombre, int edad, int altura) {
		this.nombre = nombre;
		this.edad = edad;
		this.altura = altura;
	}

	public String getNombre() {
		return nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public Integer getAltura() {
		return altura;
	}

	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", edad=" + edad + ", altura=" + altura + "]";
	}

}